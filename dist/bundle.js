/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _waxDom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./waxDom */ \"./src/waxDom.js\");\n\r\n\r\nvar waxML = {\r\n    waxDoms: [],\r\n    init: function()  {\r\n        this.parseDocument();\r\n    },\r\n    parseDocument: function() {\r\n        var forms = document.querySelectorAll('form');\r\n\r\n        // Collect all forms and create corresponding waxDoms\r\n        for (const key in forms) {\r\n            if (forms.hasOwnProperty(key)) {\r\n                const form = forms[key];\r\n                var tempDom = new _waxDom__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\r\n                tempDom.form = form;\r\n                this.waxDoms.push(tempDom);\r\n            }\r\n        }\r\n        // Add dummyWaxDom\r\n        var dummyWaxDom = new _waxDom__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\r\n        this.waxDoms.unshift(dummyWaxDom);\r\n\r\n        var inputElements = this.collectInputs();\r\n        for (const key in inputElements) {\r\n            if (inputElements.hasOwnProperty(key)) {\r\n                const element = inputElements[key];\r\n                this.pushToCorrespondingWaxDom(element)\r\n            }\r\n        }\r\n\r\n        var nonInputElements = this.collectNonInputs();\r\n        for (const key in nonInputElements) {\r\n            if (nonInputElements.hasOwnProperty(key)) {\r\n                const element = nonInputElements[key];\r\n                this.pushToCorrespondingWaxDom(element)\r\n            }\r\n        }\r\n    },\r\n    collectInputs: function() {\r\n        var selector = 'input, radio, button, checkbox, a'\r\n        return document.querySelectorAll(selector);\r\n    },\r\n    collectNonInputs: function() {\r\n        var selector = 'span, div, label, td, p'\r\n        return document.querySelectorAll(selector);\r\n    },\r\n    pushToCorrespondingWaxDom: function(element) {\r\n        for (const key in this.waxDoms) {\r\n            if (this.waxDoms.hasOwnProperty(key) && key != 0) {\r\n                var waxDom = this.waxDoms[key];\r\n                \r\n                if(element.form == waxDom.form) {\r\n                    waxDom.inputElements.push(element);\r\n                } else if(element.closest('form') == waxDom.form) {\r\n                    waxDom.nonInputElements.push(element);\r\n                } else {\r\n                    if(('input, radio, button, checkbox, a').split(',').includes(element.tagName.toLowerCase()) != -1) {\r\n                        this.waxDoms[0].nonInputElements.push(element);\r\n                    } else {\r\n                        this.waxDoms[0].inputElements.push(element);\r\n                    }\r\n                }\r\n            }\r\n        }\r\n    }\r\n}\r\n\r\nwindow.waxML = waxML;\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/waxDom.js":
/*!***********************!*\
  !*** ./src/waxDom.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass waxDom {\r\n    constructor() {\r\n        this.inputElements = [];\r\n        this.nonInputElements = [];\r\n        this.form = null;\r\n        this.formType = null;\r\n    }\r\n}\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (waxDom);\n\n//# sourceURL=webpack:///./src/waxDom.js?");

/***/ })

/******/ });