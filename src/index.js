import waxDom from './waxDom';

var waxML = {
    waxDoms: [],
    init: function()  {
        this.parseDocument();
    },
    parseDocument: function() {
        var forms = document.querySelectorAll('form');

        // Collect all forms and create corresponding waxDoms
        for (const key in forms) {
            if (forms.hasOwnProperty(key)) {
                const form = forms[key];
                var tempDom = new waxDom();
                tempDom.form = form;
                this.waxDoms.push(tempDom);
            }
        }
        // Add dummyWaxDom
        var dummyWaxDom = new waxDom();
        this.waxDoms.unshift(dummyWaxDom);

        var inputElements = this.collectInputs();
        for (const key in inputElements) {
            if (inputElements.hasOwnProperty(key)) {
                const element = inputElements[key];
                this.pushToCorrespondingWaxDom(element)
            }
        }

        var nonInputElements = this.collectNonInputs();
        for (const key in nonInputElements) {
            if (nonInputElements.hasOwnProperty(key)) {
                const element = nonInputElements[key];
                this.pushToCorrespondingWaxDom(element)
            }
        }
    },
    collectInputs: function() {
        var selector = 'input, radio, button, checkbox, a'
        return document.querySelectorAll(selector);
    },
    collectNonInputs: function() {
        var selector = 'span, div, label, td, p'
        return document.querySelectorAll(selector);
    },
    pushToCorrespondingWaxDom: function(element) {
        for (const key in this.waxDoms) {
            if (this.waxDoms.hasOwnProperty(key) && key != 0) {
                var waxDom = this.waxDoms[key];
                
                if(element.form == waxDom.form) {
                    waxDom.inputElements.push(element);
                } else if(element.closest('form') == waxDom.form) {
                    waxDom.nonInputElements.push(element);
                } else {
                    if(('input, radio, button, checkbox, a').split(', ').includes(element.tagName.toLowerCase()) != -1) {
                        this.waxDoms[0].nonInputElements.push(element);
                    } else {
                        this.waxDoms[0].inputElements.push(element);
                    }
                }
            }
        }
    }
}

window.waxML = waxML;